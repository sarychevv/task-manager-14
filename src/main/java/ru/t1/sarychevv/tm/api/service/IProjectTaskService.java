package ru.t1.sarychevv.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
