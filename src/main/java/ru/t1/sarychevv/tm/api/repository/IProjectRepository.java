package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project create(String name, String description);

    Project create(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeOneById(String id);

    Project removeOneByIndex(Integer index);

    Integer getSize();

    Boolean existsById(String id);

}
