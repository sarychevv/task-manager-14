package ru.t1.sarychevv.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
