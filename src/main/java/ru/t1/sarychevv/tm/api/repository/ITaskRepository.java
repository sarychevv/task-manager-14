package ru.t1.sarychevv.tm.api.repository;

import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    Task create(String name, String description);

    Task create(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);

    void remove(Task task);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Integer getSize();

}
