package ru.t1.sarychevv.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
