package ru.t1.sarychevv.tm.api.controller;

public interface ICommandController {

    void showArgumentError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showInfo();

    void showCommands();

    void showArguments();

    void showHelp();

    void showWelcome();
}
